package main

import (
	"log"
	"net/http"

	"bitbucket.org/telestore/teleapi"
)

func main() {

	handler := teleapi.WebhookHandlerFunc(func(event teleapi.CallEvent) teleapi.WebhookResponse {
		if event.Name == `invite` && (event.Source == `number` || event.Source == `neighbour`) &&
			event.Vpbx == `000001` && event.Call.Type == `incoming` && event.Call.ANumber == `78127798332` {
			resp := teleapi.WebhookResponseInvite{
				Accounts: []string{
					`9993`,
				},
				Contact: `Private Person`,
			}
			return resp
		}
		return teleapi.WebhookResponseEmpty
	})

	http.Handle(`/webhook`, handler)
	log.Fatal(http.ListenAndServe(`:65005`, nil))
}
