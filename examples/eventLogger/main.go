package main

import (
	"log"
	"net/http"

	"bitbucket.org/telestore/teleapi"
	"github.com/sirupsen/logrus"
)

func main() {

	handler := teleapi.WebhookHandlerFunc(func(event teleapi.CallEvent) teleapi.WebhookResponse {
		logrus.Infof(`Received event: %s`, event.Pretty())
		return teleapi.WebhookResponseEmpty
	})

	http.Handle(`/webhook`, handler)
	log.Fatal(http.ListenAndServe(`:65005`, nil))
}
