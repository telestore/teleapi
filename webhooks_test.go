package teleapi

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestWebhookHandler(t *testing.T) {
	tests := []struct {
		contentType  string
		body         string
		errorHandler bool
		eventHandler bool
		want         int
	}{
		{contentType: ``, body: ``, errorHandler: false, want: http.StatusUnsupportedMediaType},
		{contentType: ``, body: ``, errorHandler: true, want: http.StatusNotImplemented},
		{contentType: `application/json`, body: `123`, errorHandler: false, want: http.StatusBadRequest},
		{contentType: `application/json`, body: `123`, errorHandler: true, want: http.StatusNotImplemented},
		{contentType: `application/json`, body: `{"event": "unknown"}`, eventHandler: false, want: http.StatusOK},
		{contentType: `application/json`, body: `{"event": "unknown"}`, eventHandler: true, want: http.StatusOK},
	}

	errorHandler := func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotImplemented)
	}
	eventHandler := func(event CallEvent) WebhookResponse {
		return WebhookResponseEmpty
	}

	for index, test := range tests {
		handler := WebhookHandlerFunc(nil)
		if test.errorHandler {
			handler.OnError = errorHandler
		}
		if test.eventHandler {
			handler.OnEvent = eventHandler
		}
		req, err := http.NewRequest(`POST`, `/webhooks`, bytes.NewBufferString(test.body))
		if err != nil {
			t.Errorf(`Test %d failed: request is not created, error: %s`, index, err)
			continue
		}
		req.Header.Set(`Content-Type`, test.contentType)
		rr := httptest.NewRecorder()
		handler.ServeHTTP(rr, req)
		if status := rr.Code; status != test.want {
			t.Errorf(`Test %d failed: handler returned a wrong status code: %d, want %d`, index, status, test.want)
		}
	}
}

func TestWebhookResponseInvite(t *testing.T) {
	resp := WebhookResponseInvite{
		Contact: `Private person`,
		Accounts: []string{
			`1000`,
			`1001`,
		},
	}
	wantedBody := `{"accounts":["1000","1001"],"contact":"Private person"}`
	wantedContentType := `application/json`
	body := string(resp.Body())
	if body != wantedBody {
		t.Errorf(`Wrong body: '%s', want: '%s'`, body, wantedBody)
	}
	contentType := resp.ContentType()
	if contentType != wantedContentType {
		t.Errorf(`Wrong content-type: '%s', want: '%s'`, contentType, wantedContentType)
	}
}
