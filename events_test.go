package teleapi

import (
	"testing"
	"time"
)

func TestCallEventPretty(t *testing.T) {
	answerTime, _ := time.Parse(`2006-01-02`, `2019-10-30`)
	hangupTime, _ := time.Parse(`2006-01-02`, `2029-12-31`)
	startTime, _ := time.Parse(`2006-01-02`, `2004-02-29`)
	event := CallEvent{
		Call: CallInfo{
			ANumber:         `78121112233`,
			Account:         `1000`,
			AnswerTimestamp: answerTime,
			BNumber:         `74958772332`,
			Fax:             `fax`,
			HangupCause:     `Normal clearing`,
			HangupTimestamp: hangupTime,
			Id:              `call-Id`,
			Neighbour:       `neighbour`,
			RNumber:         `74998770000`,
			Record:          `record`,
			StartTimestamp:  startTime,
			Type:            `incoming`,
			Voicemail:       `voicemail`,
		},
		Name:   `invite`,
		Source: `number`,
		Vpbx:   `000001`,
	}
	pretty := event.Pretty()
	want := `{
  "call": {
    "a_number": "78121112233",
    "account": "1000",
    "answer_timestamp": "2019-10-30T00:00:00Z",
    "b_number": "74958772332",
    "fax": "fax",
    "hangup_cause": "Normal clearing",
    "hangup_timestamp": "2029-12-31T00:00:00Z",
    "id": "call-Id",
    "neighbour": "neighbour",
    "r_number": "74998770000",
    "record": "record",
    "start_timestamp": "2004-02-29T00:00:00Z",
    "type": "incoming",
    "voicemail": "voicemail"
  },
  "event": "invite",
  "source": "number",
  "vpbx": "000001"
}`
	if pretty != want {
		t.Errorf(`Wrong pretty output of event: %s`, pretty)
	}
}
