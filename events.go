package teleapi

import (
	"encoding/json"
	"time"
)

type CallEvent struct {
	Call   CallInfo `json:"call"`
	Name   string   `json:"event"`
	Source string   `json:"source"`
	Vpbx   string   `json:"vpbx"`
}

func (e CallEvent) Pretty() string {
	result, _ := json.MarshalIndent(e, ``, `  `)
	return string(result)
}

type CallInfo struct {
	ANumber         string    `json:"a_number"`
	Account         string    `json:"account"`
	AnswerTimestamp time.Time `json:"answer_timestamp"`
	BNumber         string    `json:"b_number"`
	Fax             string    `json:"fax"`
	HangupCause     string    `json:"hangup_cause"`
	HangupTimestamp time.Time `json:"hangup_timestamp"`
	Id              string    `json:"id"`
	Neighbour       string    `json:"neighbour"`
	RNumber         string    `json:"r_number"`
	Record          string    `json:"record"`
	StartTimestamp  time.Time `json:"start_timestamp"`
	Type            string    `json:"type"`
	Voicemail       string    `json:"voicemail"`
}
