package teleapi

import (
	"encoding/json"
	"net/http"
)

type WebhookResponse interface {
	Body() []byte
	ContentType() string
}

type WebhookResponseSimple string

func (r WebhookResponseSimple) Body() []byte {
	return []byte(r)
}

func (r WebhookResponseSimple) ContentType() string {
	return `application/json`
}

const WebhookResponseEmpty WebhookResponseSimple = ``

type WebhookResponseInvite struct {
	Accounts []string `json:"accounts"`
	Contact  string   `json:"contact"`
}

func (r WebhookResponseInvite) Body() []byte {
	result, _ := json.Marshal(r)
	return result
}

func (r WebhookResponseInvite) ContentType() string {
	return `application/json`
}

type WebhookHandler struct {
	OnError func(w http.ResponseWriter, r *http.Request)
	OnEvent func(event CallEvent) WebhookResponse
}

func (h *WebhookHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get(`Content-Type`) != `application/json` {
		if h.OnError == nil {
			w.WriteHeader(http.StatusUnsupportedMediaType)
			return
		}
		h.OnError(w, r)
		return
	}
	var event CallEvent
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&event)
	if err != nil {
		if h.OnError == nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		h.OnError(w, r)
		return
	}
	if h.OnEvent == nil {
		return
	}
	resp := h.OnEvent(event)
	w.Header().Set(`Content-Type`, resp.ContentType())
	w.Write(resp.Body())
}

func WebhookHandlerFunc(eventHandler func(event CallEvent) WebhookResponse) *WebhookHandler {
	return &WebhookHandler{
		OnEvent: eventHandler,
	}
}
